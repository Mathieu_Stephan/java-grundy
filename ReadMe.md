## Grundy programmé en Java
Ce programme est un jeu de Grundy, une variation du jeu de Nim écrit en Java dans le cadre d'un projet (SAE 1.01) de mes études d'informatique. Vous trouverez les fichiers *.java* des différentes versions ainsi que les *.class* directement exécutable. Dans le dossier Explication se trouve des exemples de parties et des explications sur la création du jeu. La version 1 du jeu est une version joueur contre joueur, la version 2 inclus une intelligence artificielle.

## Comment jouer ?


 1. Télécharger le dossier class et aller à l'intérieur à l'aide d'un inviter de commande
 2. exécuter la commande suivante:
 3. ``` java  Start <nom du programme>```

Vous pouvez jouer a Grundy1 ou Grundy2.


## Grundy programmed in Java

 

This program is a Grundy game, a variation of Nim's game written in Java as part of a project (SAE 1.01) of my computer science studies. You will find the .java files of the different versions as well as the .class directly executable. In the Explication folder are examples of games and explanations about how to create the game. Version 1 of the game is a player-versus-player version, version 2 includes artificial intelligence. 


## How to play?

 1. Download the class folder and go inside using a command prompt
 2. run the following command:
 3. ```java Start <name of programm>```

 You can play Grundy1 or Grundy2.
