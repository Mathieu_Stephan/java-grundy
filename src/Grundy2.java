/***
* Jeux de grundy en joueur contre ordinateur
* @author Stephan Mathieu
*/
import java.util.Arrays; 
import java.util.Random;

class Grundy2{
	final int[] nbGrundy = { 1, 0, 0, 1, 0, 2, 1, 0, 2, 1, 0, 2, 1, 3, 2, 1, 3, 2, 4, 3, 0, 4, 3, 0, 4, 3, 0, 4, 1, 2, 3, 1, 2, 4, 1, 2, 4, 1, 2, 4, 1, 5, 4, 1, 5, 4, 1, 5, 4, 1, 0, 2, 1, 0, 2, 1, 5, 2, 1, 3, 2, 1, 3, 2, 4, 3, 2, 4, 3, 2, 4, 3, 2, 4, 3, 2, 4, 3, 2, 4, 5, 2, 4, 5, 2, 4, 3, 7, 4, 3, 7, 4, 3, 7, 4, 3, 5, 2, 3, 5, 2, 3, 5, 2, 3, 5, 2, 3, 5, 2, 3, 5, 2 }; 
	// nombre de grundy du jeux de grundy d'après Eric M. Schmidt sur son programme (https://oeis.org/A002188) , la 0 ème valeur à été changé pour éviter les bugs.
		void principal(){
			gaming();
		}
		
		/**
		* affiche un nombre d'allumette défini par l'utilisateur
		* @param i le nombre d'allumette qu'on veut afficher
		*/
		void dessinAllu(int i){
			int j = 0;
			while(j < i){
				System.out.print("| ");
				j++;
			}
			System.out.print("---" + j);
			System.out.println("");
		}
		
		/**
		* Test pour voir si dessinAllu affiche le bon nombre d'allumette
		*/
		void testDessinAllu (int i) {
			dessinAllu(i);
		}
		/**
		 * Créer une liste d'int, ces int sont le nombre d'allumettes et leur index dans la liste est le numéro du tas
		 * @param tasActu, la liste actuelle d'allumette
		 * @param divis, nombre d'allumette à séparer (qui resteras à sa posotion originelle)
		 * @param allu, le nombre d'allumettes total sur la ligne selectionner
		 * @param ligneSelect, la ligne choisi par le joueur
		 */
		int[] divisTas(int[] tasActu, int divis, int allu, int ligneSelect){
			int[] res = new int[tasActu.length + 1];
			int i = 0;
			while(i < tasActu.length){
				if(i == ligneSelect){
					res[i] = divis;
				}else{
					res[i] = tasActu[i];
				}	
				i++;			
			}
			res[ligneSelect] = divis;
			res[res.length - 1] = allu - divis;
			return res;
		}
		/**
		* Test pour voir si divisTas créer bien une nouvel liste contenant les allumettes de la liste précédente + une nouvelle ligne
		*/
		void testDivisTas (int[] tasActu, int divis, int allu, int ligneSelect, int[] result) {
			// Arrange
			System.out.print ("On suppose que divisTas(" + Arrays.toString(tasActu) + ", " + divis + ", " + allu + ", " + ligneSelect + ") vas donner " + Arrays.toString(result) );
			// Act
			int[] resExec = divisTas(tasActu,divis,allu,ligneSelect);
			// Assert
			if (Arrays.equals(result, resExec)){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		/**
		 * Affiche le jeu : le numéro de la ligne suivi des allumette
		 * @param tasActu, la liste actuelle d'allumette
		 */
		void affichJeu(int[] tasActu){
			for(int j = 0; j < tasActu.length; j++){
				System.out.print(j + " : ") ;
				dessinAllu(tasActu[j]);
			}
		}
		
		/**
		* Test pour voir si affichJeu affiche bien la partie
		*/
		void testAffichJeu (int[] i) {
			affichJeu(i);
		}
		
		/**
		 * fait jouer les joueurs
		 * @param tasActu, la liste actuelle d'allumette
		 * @param joueurActu, le joueur qui joue actuellement
		 * @param j1,j2, les joueurs
		 * @return une nouvelle liste d'allumette
		 * 
		 */
		int[] jeu(int[] tasActu, int joueurActu , String j1 ,String j2){
			int[] res;
			if(joueurActu == 2){
				int ligneJouée = SimpleInput.getInt(j2 + ", veuillez indiquer la ligne que vous souhaitez jouer: ");
				while (ligneJouée < 0 || ligneJouée >= tasActu.length  || tasActu[ligneJouée] <= 2){
					ligneJouée = SimpleInput.getInt(j2 + ", veuillez indiquer la ligne que vous souhaitez jouer: ");
				}
				int divis = SimpleInput.getInt(j2 + ", veuillez indiquer le nombre d'allumettes à séparer : ");
				while (divis <= 0 || tasActu[ligneJouée] - divis == divis || tasActu[ligneJouée] - divis == 0 || divis >= tasActu[ligneJouée]){
					divis = SimpleInput.getInt(j2 + ", veuillez indiquer le nombre d'allumettes à séparer : ");
				}
				res = divisTas(tasActu,divis , tasActu[ligneJouée], ligneJouée);
			}else{
				int ligneJouée = SimpleInput.getInt(j1 + ", veuillez indiquer la ligne que vous souhaitez jouer: ");
				while (ligneJouée < 0 || ligneJouée >= tasActu.length || tasActu[ligneJouée] <= 2){
					ligneJouée = SimpleInput.getInt(j1 + ", veuillez indiquer la ligne que vous souhaitez jouer: ");
				}
				int divis = SimpleInput.getInt(j1 + ", veuillez indiquer le nombre d'allumettes à séparer : ");
				while (divis <= 0 || tasActu[ligneJouée] - divis == divis || tasActu[ligneJouée] - divis == 0 || divis >= tasActu[ligneJouée]){
					divis = SimpleInput.getInt(j1 + ", veuillez indiquer le nombre d'allumettes à séparer : ");
				}
				res = divisTas(tasActu,divis , tasActu[ligneJouée], ligneJouée);
			}
			return res;
		}
		
		
		/**
		* Test pour voir si jeu créer bien une nouvel liste contenant les allumettes et utilise bien la méthode divisTas
		*/
		void testJeu (int[] tasActu, int joueurActu , String j1 ,String j2, int[] result) {
			// Arrange
			System.out.print ("On suppose que jeu(" + Arrays.toString(tasActu) + ", " + joueurActu + ", " + j1 + ", " + j2 + ") vas donner " + Arrays.toString(result) );
			// Act
			int[] resExec = jeu(tasActu,joueurActu,j1,j2);
			// Assert
			if (Arrays.equals(result, resExec)){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		
		/**
		 * fait jouer le joueur et l'ordi
		 * @param tasActu, la liste actuelle d'allumette
		 * @param joueurActu, le joueur qui joue actuellement
		 * @param j, le joueurs
		 * @return une nouvelle liste d'allumette
		 * 
		 */
		int[] jeuPve(int[] tasActu, int joueurActu , String j){
			int[] res;
			if(joueurActu == 2){
				int ligneJouée = SimpleInput.getInt(j + ", veuillez indiquer la ligne que vous souhaitez jouer: ");
				while (ligneJouée < 0 || ligneJouée >= tasActu.length  || tasActu[ligneJouée] <= 2){
					ligneJouée = SimpleInput.getInt(j + ", veuillez indiquer la ligne que vous souhaitez jouer: ");
				}
				int divis = SimpleInput.getInt(j + ", veuillez indiquer le nombre d'allumettes à séparer : ");
				while (divis <= 0 || tasActu[ligneJouée] - divis == divis || tasActu[ligneJouée] - divis == 0 || divis >= tasActu[ligneJouée]){
					divis = SimpleInput.getInt(j + ", veuillez indiquer le nombre d'allumettes à séparer : ");
				}
				res = divisTas(tasActu,divis , tasActu[ligneJouée], ligneJouée);
			}else{
				System.out.println("Au tour de l'ordi : ");
				res = coupOrdi(tasActu);
			}
			return res;
		}
		
		/**
		* Test pour voir si jeuPve créer bien une nouvel liste contenant les allumettes et utilise bien la méthode divisTas comme jeu mais avec un ordi a la place d'un joueur
		*/
		void testJeuPve (int[] tasActu, int joueurActu , String j, int[] result) {
			// Arrange
			System.out.print ("On suppose que jeuPve(" + Arrays.toString(tasActu) + ", " + joueurActu + ", " + j + ") vas donner " + Arrays.toString(result) );
			System.out.println("");
			// Act
			int[] resExec = jeuPve(tasActu,joueurActu,j);
			// Assert
			if (Arrays.equals(result, resExec)){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		/**
		 * verifie si au moins une ligne est jouable dans une liste d'allumette
		 * @param tasActu, la liste actuelle d'allumette
		 * 
		 */
		boolean verif(int[] tasActu){
			boolean res = false;
			for(int i = 0; i < tasActu.length;i++){
				if(tasActu[i] > 2){
					res = true;
				}
			}
			return res;
		}
		
		/**
		* Test pour voir si verif vérifie bien que la liste fournie est jouable.
		*/
		void testVerif (int[] tasActu, boolean result) {
			// Arrange
			System.out.print ("On suppose que verif(" + Arrays.toString(tasActu)  + ") vas donner " + result );
			// Act
			boolean resExec = verif(tasActu);
			// Assert
			if ( resExec == result){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		/**
		 * Calcule quel ligne et quel nombre d'allumette l' Ordi doit choisir pour gagner grâce au deux méthodes suivantes.
		 * @param tasActu, le jeu actuel
		 * @return le jeu après que l'Ordi ai jouer.
		 */
		int[] coupOrdi(int[] tasActu){
			int i = 0;
			int ligneSelect = 0;
			boolean check = true;
			int[] valid = {4,2,1};
			int minus = 1;
			while(i < tasActu.length && check){
				int j = 0;
				while(j < valid.length && check){
					minus = valid[j];
					if(tasActu[i] > 3 && coupSuivantEstGagnant(tasActu, ligneSelect, minus)){
						check = false;
						ligneSelect = i;
					}
					j++;
				}
				i++;
			}
			if(check){
				for(int j = 0; j < tasActu.length; j++){
					if(tasActu[j] > 2){
						ligneSelect = j;
						minus = 2;
					}
				}
			}
			if(tasActu[ligneSelect] == 4){
				minus = 1;
			}
			System.out.println("L'ordi choisi la ligne " + ligneSelect + " et la divise de " + minus + " allumette(s)");
			return divisTas(tasActu, minus, tasActu[ligneSelect], ligneSelect);
		}
		
		/**
		* Test pour voir si coupOrdi donne bien un jeu gagnant
		*/
		void testCoupOrdi (int[] tasActu, int[] result) {
			// Arrange
			System.out.print ("On suppose que coupOrdi(" + Arrays.toString(tasActu)  + ") vas donner " + Arrays.toString(result) );
			// Act
			int[] resExec = coupOrdi(tasActu);
			// Assert
			if ( Arrays.equals(resExec,result)){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		/**
		 * Calcule si le coup suivant (si on joue une allumette) est gagnant
		 * @param tasActu le jeu actuel
		 * @param ligneJouée, la ligne jouée (index de la liste tasActu)
		 * @param minus, les allumettes qu'on veut enlever
		 * @return true si le coup suivant est gagnant, false sinon.
		 */
		boolean coupSuivantEstGagnant(int[] tasActu, int ligneJouée, int minus){
			boolean res = false;
			if(grundy(tasActu[ligneJouée], minus) == 0 && tasActu[ligneJouée] > 2){
				res = true;
			}
			return res;
		}
		
		/**
		* Test pour voir si coupSuivantEstGagnant donne bien true si le coup suivant est gagnant
		*/
		void testCoupSuivantEstGagnant (int[] tasActu, int ligneJouée, int minus, boolean result) {
			// Arrange
			System.out.print ("On suppose que coupSuivantEstGagnant(" + Arrays.toString(tasActu) + ", " + ligneJouée + ", " + minus + ") vas donner " + result + " " );
			// Act
			boolean resExec = coupSuivantEstGagnant(tasActu,ligneJouée,minus);
			// Assert
			if (resExec == result){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		/**
		 * Donne 0 si un nombre d'allumette est gagnant pour le joueur qui vient de jouer. Utilise la constante nbGrundy qui contient les 112 premiers nombres de grundy du jeux (exepté le 0 qui est invalide)
		 * @param valeur, le nombre d'allumette
		 * @param minus, qui sers à determiner si le nombre d'allumette qu'on soustrait donnera un résultat gagnant.
		 * @return une valeur, le nombre de grundy du nombre d'allumette choisi moins les allumette qu'on souhaite enlever.
		 */
		int grundy(int valeur, int minus){
			int res  = 1;
			if (valeur - minus >= 0 && valeur - minus < nbGrundy.length){
				res = nbGrundy[valeur - minus];
			}
			return res;
		}
		
		/**
		* Test pour voir si grundy renvoi le nombre de grundy de la valeur entrée moins minus
		*/
		void testGrundy (int valeur, int minus, int result) {
			// Arrange
			System.out.print ("On suppose que grundy("  + valeur + ", " + minus + ") vas donner " + result + " ");
			// Act
			int resExec = grundy(valeur,minus);
			// Assert
			if (resExec == result){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		
		/**
		 * Permet de démarrer une partie
		 */
		void gaming (){
			int nbAllu = SimpleInput.getInt("Nombre d'allumette (supérieur à 3, sinon le jeu n'a aucun sens et inférieur à 112, sinon le terminal ne l'affiche pas sur une seule ligne ");
			while(nbAllu < 4 || nbAllu > 112 ){
				nbAllu = SimpleInput.getInt("Veuillez mettre un nombre supérieur à 3 et inférieur à 112 ...  ");
			}
			int partie = SimpleInput.getInt("Tapez 0 pour jouer contre un autre joueur, tapez 1 pour jouer contre un ordinateur :  ");
			while( partie < 0 || partie > 1){
				partie = SimpleInput.getInt("Les seules valeurs acceptées sont 0 (joueur contre joueur) et 1 (joueur contre ordi) : ");
			}
			if(partie == 0){
				gamingPvp(nbAllu);
			}else{
				gamingPve(nbAllu);
			}
			
	}
		/**
		* Test pour voir si gaming permet bien de démarrer une partie
		*/
		void testGaming(){
			gaming();
		}
		/**
		 * Permet de jouer au jeu du Grundy contre un autre joueur
		 * @param nbAllu, le nombre d'allumette avec lesquels on veut jouer
		 */
		void gamingPvp(int nbAllu){
			int ligneJouée = 0;
			int joueurActu = 2;
			int winner = 1;
			String j1 = SimpleInput.getString("Premier joueur : ");
			String j2 = SimpleInput.getString("Deuxième joueur : ");
			int[] tasActu = {nbAllu};
			affichJeu(tasActu);
			int divis = SimpleInput.getInt(j1 + ", veuillez indiquer le nombre d'allumettes à séparer : ");
			while (divis <= 0 || tasActu[ligneJouée] - divis == divis || tasActu[ligneJouée] - divis == 0 || divis >= tasActu[ligneJouée]){
						divis = SimpleInput.getInt(j1 + ", veuillez indiquer un nombre valide d'allumettes à séparer : ");
			}
			tasActu = divisTas(tasActu,divis , tasActu[ligneJouée], ligneJouée);
			affichJeu(tasActu);
			int[] res = {0};
			while(verif(tasActu)){
				res = jeu(tasActu,joueurActu,j1, j2);
				affichJeu(res);
				tasActu = res;
				if(joueurActu == 2){
					joueurActu = 1;
					winner = 2;
				}else{
					joueurActu = 2;
					winner = 1;
				}
			}
			if(winner == 1){
				System.out.println("La victoire est pour : " + j1 + " ! Félicitation !");
				System.out.println("Dommage " + j2 + ", plus de chance la prochaine fois !");
				
			}else{
				System.out.println("La victoire est pour : " + j2 + " ! Félicitation !");
				System.out.println("Dommage " + j1 + ", plus de chance la prochaine fois !");
			}
		}
		
		/**
		* Test pour voir si gamingPvp permet bien de jouer contre un joueur
		*/
		void testGamingPvp(int nbAllu){
			gamingPvp(nbAllu);
		}
		
		/**
		 * Permet de jouer au jeu du Grundy contre un Ordi
		 * @param nbAllu, le nombre d'allumette avec lesquels on veut jouer
		 */
		void gamingPve(int nbAllu){
			int ligneJouée = 0;
			int joueurActu = 1;
			int winner = 1;
			int premier = SimpleInput.getInt("Tapez 0 pour jouer en premier, tapez 1 pour que l'ordi joue en premier :  ");
			while( premier < 0 || premier > 1){
				premier = SimpleInput.getInt("Les seules valeurs acceptées sont 0 (joueur en premier) et 1 (ordi en premier) :  ");
			}
			String j = SimpleInput.getString("Veuillez entrer le nom du joueur : ");
			int[] tasActu = {nbAllu};
			affichJeu(tasActu);
			if(premier == 1){
				System.out.println("L'ordinateur joue...");
				tasActu = coupOrdi(tasActu);
				affichJeu(tasActu);
				joueurActu = 2;
			}else{
				int divis = SimpleInput.getInt(j + ", veuillez indiquer le nombre d'allumettes à séparer : ");
				while (divis <= 0 || tasActu[ligneJouée] - divis == divis || tasActu[ligneJouée] - divis == 0 || divis >= tasActu[ligneJouée]){
					divis = SimpleInput.getInt(j + ", veuillez indiquer un nombre valide d'allumettes à séparer : ");
				}
				tasActu = divisTas(tasActu,divis , tasActu[ligneJouée], ligneJouée);
				affichJeu(tasActu);
			}
			int[] res = {0};
			while(verif(tasActu)){
				res = jeuPve(tasActu,joueurActu,j);
				affichJeu(res);
				tasActu = res;
				if(joueurActu == 2){
					joueurActu = 1;
					winner = 2;
				}else{
					joueurActu = 2;
					winner = 1;
				}
			}
			if(winner == 1){
				System.out.println("La victoire est pour l' Ordinateur");
				System.out.println("Dommage " + j + ", plus de chance la prochaine fois !");
				
			}else{
				System.out.println("La victoire est pour : " + j + " ! Félicitation !");
			}
		}
		
		/**
		* Test pour voir si gamingPve permet bien de jouer contre un ordi
		*/
		void testGamingPve(int nbAllu){
			gamingPve(nbAllu);
		}
		
}
