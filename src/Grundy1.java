/***
* Jeux de grundy en joueur contre joueur
* @author Stephan Mathieu
*/
import java.util.Arrays; 

class Grundy1{
		void principal(){
			gaming();
		}
		
		/**
		* affiche un nombre d'allumette défini par l'utilisateur
		* @param i le nombre d'allumette qu'on veut afficher
		*/
		void dessinAllu(int i){
			int j = 0;
			while(j < i){
				System.out.print("| ");
				j++;
			}
			System.out.print("---" + j);
			System.out.println("");
		}
		
		/**
		* Test pour voir si dessinAllu affiche le bon nombre d'allumette
		*/
		void testDessinAllu (int i) {
			dessinAllu(i);
		}
		/**
		 * Créer une liste d'int, ces int sont le nombre d'allumettes et leur index dans la liste est le numéro du tas
		 * @param tasActu, la liste actuelle d'allumette
		 * @param divis, nombre d'allumette à séparer (qui resteras à sa posotion originelle)
		 * @param allu, le nombre d'allumettes total sur la ligne selectionner
		 * @param ligneSelect, la ligne choisi par le joueur
		 */
		int[] divisTas(int[] tasActu, int divis, int allu, int ligneSelect){
			int[] res = new int[tasActu.length + 1];
			int i = 0;
			while(i < tasActu.length){
				if(i == ligneSelect){
					res[i] = divis;
				}else{
					res[i] = tasActu[i];
				}	
				i++;			
			}
			res[res.length - 1] = allu - divis;
			return res;
		}
		/**
		* Test pour voir si divisTas créer bien une nouvel liste contenant les allumettes de la liste précédente + une nouvelle ligne
		*/
		void testDivisTas (int[] tasActu, int divis, int allu, int ligneSelect, int[] result) {
			// Arrange
			System.out.print ("On suppose que divisTas(" + Arrays.toString(tasActu) + ", " + divis + ", " + allu + ", " + ligneSelect + ") vas donner " + Arrays.toString(result) );
			// Act
			int[] resExec = divisTas(tasActu,divis,allu,ligneSelect);
			// Assert
			if (Arrays.equals(result, resExec)){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		/**
		 * Affiche le jeu : le numéro de la ligne suivi des allumette
		 * @param tasActu, la liste actuelle d'allumette
		 */
		void affichJeu(int[] tasActu){
			for(int j = 0; j < tasActu.length; j++){
				System.out.print(j + " : ") ;
				dessinAllu(tasActu[j]);
			}
		}
		
		/**
		* Test pour voir si affichJeu affiche bien la partie
		*/
		void testAffichJeu (int[] i) {
			System.out.println(Arrays.toString(i) + " donne : ");
			affichJeu(i);
		}
		
		/**
		 * fait jouer les joueurs
		 * @param tasActu, la liste actuelle d'allumette
		 * @param joueurActu, le joueur qui joue actuellement
		 * @param j1,j2, les joueurs
		 * @return une nouvelle liste d'allumette
		 * 
		 */
		int[] jeu(int[] tasActu, int joueurActu , String j1 ,String j2){
			int[] res;
			if(joueurActu == 2){
				int ligneJouée = SimpleInput.getInt(j2 + " veuillez indiquer la ligne que vous souhaitez jouer: ");
				while (ligneJouée < 0 || ligneJouée >= tasActu.length  || tasActu[ligneJouée] <= 2){
					ligneJouée = SimpleInput.getInt(j2 + " veuillez indiquer la ligne que vous souhaitez jouer: ");
				}
				int divis = SimpleInput.getInt(j2 + " veuillez indiquer le nombre d'allumettes à séparer : ");
				while (divis <= 0 || tasActu[ligneJouée] - divis == divis || tasActu[ligneJouée] - divis == 0 || divis >= tasActu[ligneJouée]){
					divis = SimpleInput.getInt(j2 + " veuillez indiquer le nombre d'allumettes à séparer : ");
				}
				res = divisTas(tasActu,divis , tasActu[ligneJouée], ligneJouée);
			}else{
				int ligneJouée = SimpleInput.getInt(j1 + " veuillez indiquer la ligne que vous souhaitez jouer: ");
				while (ligneJouée < 0 || ligneJouée >= tasActu.length || tasActu[ligneJouée] <= 2){
					ligneJouée = SimpleInput.getInt(j1 + " veuillez indiquer la ligne que vous souhaitez jouer: ");
				}
				int divis = SimpleInput.getInt(j1 + " veuillez indiquer le nombre d'allumettes à séparer : ");
				while (divis <= 0 || tasActu[ligneJouée] - divis == divis || tasActu[ligneJouée] - divis == 0 || divis >= tasActu[ligneJouée]){
					divis = SimpleInput.getInt(j1 + " veuillez indiquer le nombre d'allumettes à séparer : ");
				}
				res = divisTas(tasActu,divis , tasActu[ligneJouée], ligneJouée);
			}
			return res;
		}
		/**
		* Test pour voir si jeu créer bien une nouvel liste contenant les allumettes et utilise bien la méthode divisTas
		*/
		void testJeu (int[] tasActu, int joueurActu , String j1 ,String j2, int[] result) {
			// Arrange
			System.out.print ("On suppose que jeu(" + Arrays.toString(tasActu) + ", " + joueurActu + ", " + j1 + ", " + j2 + ") vas donner " + Arrays.toString(result) );
			System.out.println("");
			// Act
			int[] resExec = jeu(tasActu,joueurActu,j1,j2);
			// Assert
			if (Arrays.equals(result, resExec)){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		/**
		 * verifie si au moins une ligne est jouable dans une liste d'allumette
		 * @param tasActu, la liste actuelle d'allumette
		 * 
		 */
		boolean verif(int[] tasActu){
			boolean res = false;
			for(int i = 0; i < tasActu.length;i++){
				if(tasActu[i] > 2){
					res = true;
				}
			}
			return res;
		}
		
		/**
		* Test pour voir si verif vérifie bien que la liste fournie est jouable.
		*/
		void testVerif (int[] tasActu, boolean result) {
			// Arrange
			System.out.print ("On suppose que verif(" + Arrays.toString(tasActu)  + ") vas donner " + result + " ");
			// Act
			boolean resExec = verif(tasActu);
			// Assert
			if ( resExec == result){
			System.out.println ("OK");
			} else {
			System.err.println ("ERREUR");
			}
		}
		
		/**
		 * Permet de jouer au jeu
		 */
		void gaming (){
			int ligneJouée = 0;
			int joueurActu = 2;
			int winner = 1;
			int nbAllu = SimpleInput.getInt("Nombre d'allumette (supérieur à 3, sinon le jeu n'a aucun sens et inférieur à 112, sinon le terminal ne l'affiche pas sur une seule ligne ");
			while(nbAllu < 4 || nbAllu > 112 ){
				nbAllu = SimpleInput.getInt("Veuillez mettre un nombre supérieur à 3 et inférieur à 112 ...  ");
			}
			String j1 = SimpleInput.getString("Premier joueur : ");
			String j2 = SimpleInput.getString("Deuxième joueur : ");
			int[] tasActu = {nbAllu};
			affichJeu(tasActu);
			int divis = SimpleInput.getInt(j1 + " veuillez indiquer le nombre d'allumette à séparer : ");
			while (divis <= 0 || tasActu[ligneJouée] - divis == divis || tasActu[ligneJouée] - divis == 0 || divis >= tasActu[ligneJouée]){
						divis = SimpleInput.getInt(j1 + " veuillez indiquer le nombre d'allumette à séparer : ");
			}
			tasActu = divisTas(tasActu,divis , tasActu[ligneJouée], ligneJouée);
			affichJeu(tasActu);
			int[] res = {0};
			while(verif(tasActu)){
				res = jeu(tasActu,joueurActu,j1, j2);
				affichJeu(res);
				tasActu = res;
				if(joueurActu == 2){
					joueurActu = 1;
					winner = 2;
				}else{
					joueurActu = 2;
					winner = 1;
				}
			}
			if(winner == 1){
				System.out.println("la victoire est pour : " + j1 + " !");
			}else{
			System.out.println("la victoire est pour : " + j2 + " !");
		}
	}
		/**
		* Test pour voir si gaming permet bien de jouer
		*/
		void testGaming(){
			gaming();
		}
}
